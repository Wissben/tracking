# Tracking of the learning process
### Wissam Benhaddad

## Day 1 & 2 :

Currently I was tasked to start my learning process, my coworkers provided me with a very nice and structured guide.
In priority i started with the LearnPython tutorial, providing insights for reading and coding a **Pythonic**
code. I present the following list describing my progression :
- [x] Isolating dependencies with virtual environements
- [x] Strucuting python project :
	- [x] Using well known template as base for project
	- [x] Notion of Modules,Packages
	- [x] Object Oriented Programming paradygm 
- [x] Coding in a pythonc style and writing good documentation for functionalities
- [x] Unit testing with basic python packages (unittest, Doctest, Hypthosesis)
- [x] Logging for debugging purposes
- [x] Common traps 
- [ ] Scenarios for Python applications : 
	- [x] Network application
	- [] Web applications ( currently I'm trying to deploy a django app to gather data from internet users using Django as backend and Eldarion as PaaS for deployement)
	- [x] HTML Scraping
	- [ ] CLI's
	- [ ] GUI's
## Day 4 to 8 :
- [ ] Flask Mega tutorial :
	These past few days I followed the Flask-Mega-Tutorial step by step to make a micro-bloging application :
	- [x] Chapter 1 : Hello word ! , in this chapter I discovered how to install the flask package, and how to structure a flask project in such a way that it can stay maintainble and tracable over the time, separating the views from their logic was an important step. the topic of routing was also developed in this chapter.
	- [x] Chapter 2 : Templates, the idea of templates is to write the minimum amount of html code to avoid repetition and promote reusability, using the Jinja2 templating engine helps to define dynamic components to the pages
	- [x] Chapter 3 : Web forms, the main goal was to design an interactive login/logout/signup/edit forms through the usage of the flask-forms module.
	- [x] Chapter 4 : Database : Using the popular SQLAlchemy package to quickly design a model, proposed as an Object Relational Mapper, SQLAlchemy simplifies the query process for the database, by query I mean : Insert, Update, Delete, Retrieve. The principle of database migrations was also introduced to ease up the database maintainablity.
	- [x] Chapter 5 : User Logins, Using the Flask-login extension made the process of sessions management very easy, as long as there is a reliable database structure, the application can keep trace of logged in users across various routes, and also requires the auhtentification of certain users before accessing certain routes, linking the data submitted to a simple database to prevent chaotic behaviour and maintain the current user logged in as long as he didn't log out is the main goal of this chapter. 
	- [x] Chapter 6: Profile Page and Avatarsn in this chapter I've managed to create a generic profile page for authenticated users displaying for now dummy data ( fake posts )
	- [x] Chapter 7: Error Handling, the handling of errors is done by redirecting known errors event to templated error views, the user shouldn't be notified of the details of the error, such details are instead redirected to log files or sent via emails to system administrators, the both ways were implemented in this chapter.
	- [x] Chapter 8: Followers, in this chapter I enhanced the database structure with the **follower** relationship to store the links between users. so that a user will see in his home page the posts of the users he follows only. A small section was dedicated to Unit testing althout it was kind of minimalistic.
	- [x] Chapter 9 : Pagination, Basically this small chapter teaches us how to load small portions of the data fetched from the database to display them in a nice paginated way to the user.
	- [x] Chapter 10 : Email Support, I've learned how to design a password change request protocol to let the user change his password, this is done by using the flask-email extension and the jwt encryption method.
	- [x] Chapter1 11 : Facelift, Pure front end part where I've learned to use the bootstrap extension and encapsulate the templates into a nice view and enhance the UX.
	- [x] Chapter 12 : Dates and Times, This part is all about time format standardization, displaying the right format for the users all around the world, using the moment.js library.
	- [x] Chapter 13 : I18n and L10n, This part is an important part for the internationalization of a web app, it show how to use the babel library for automatic template and rendering translation using static ressources, i.e the translation of each fixed strings is stored into a special file, which will be querried by Babel when it encounters a special tag for translation '_'.
	- [x] Chapter 14 :  Ajax, I've learned how to balance the work between client and server, using Asynchrous request.


- [x] Mr.BOUKERBOUB tasked me to develop some features to my app that are external to the microbloging ecosystem : 
	- [x] Uploading an audio file to the server and return a delayed computation.
	- [x] Uploading an audio file to the server and process the audio file second by second in the server side using the stream_template Class of Jinja2.
	- [x] Real time audio parsing using the client side microphone, I've completed this task for now, i'm able record sound from the user's microphone and send the raw data (bytes) to the server, currently the server returns a dummy probability. The idea is to record the sound and send the data each second to the flask server, which will at its turn make a request to a tensorflow_served model via http request using the protobuf format and send the prediction back to the user.
	- [ ] Real time audio features extraction : Currently i'm learning a lot of theorical background about audio signal processing on computers such as how the data is represented, how it is processed. My goal for the next week is to learn how the MFCC features extraction work to apply it to the audio signal that comes from the user, this will help encoding the information into meaningful features vectors to send it to the served model. As for now i'm stuck into the Filter_bank step of this procedure.
